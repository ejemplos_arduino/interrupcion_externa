# README - Control de Semáforo con Interrupciones Externas en Arduino

Este archivo README proporciona una breve introducción y explicación del código que se encuentra a continuación. El código controla los LEDs de un semáforo haciendo uso de interrupciones externas. Se utiliza un pulsador como disparador de la interrupción, que pone la luz verde durante un tiempo para permitir el paso del peatón. Luego, vuelve a ejecutar la secuencia normal del semáforo, ya sea de día o de noche.

## Descripción del Código

El siguiente código de Arduino controla un semáforo con tres LEDs (rojo, amarillo y verde) y hace uso de una interrupción externa activada por un pulsador. La secuencia del semáforo cambia dependiendo de si es de día o de noche, lo cual se determina mediante la lectura de un sensor de luz (LDR). Cuando se presiona el pulsador, se activa la secuencia del peatón.

```cpp
const int LED_ROJO = 6; // Pin del LED rojo
const int LED_AMARILLO = 5; // Pin del LED amarillo
const int LED_VERDE = 4; // Pin del LED verde

const int PIN_LDR = A0; // Pin de lectura del LDR (sensor de luz)
const int PIN_PULSADOR = 2; // Pin de lectura del pulsador
const int VELOCIDAD_CAMBIO = 500; // Tiempo de espera al cambiar el LED
const int LIMITE_CAMBIO = 900; // Límite para distinguir entre día y noche

bool secuenciaPeatón = false; // Indica si está activa la secuencia del peatón
```

- Se definen los pines de los LEDs, el pin de lectura del LDR, el pin del pulsador y algunas constantes para controlar la velocidad de cambio de los LEDs y el límite de lectura del LDR para distinguir entre día y noche.

```cpp
void setup() {
  Serial.begin(9600); // Inicializa la consola
  
  pinMode(LED_ROJO, OUTPUT);
  pinMode(LED_AMARILLO, OUTPUT);
  pinMode(LED_VERDE, OUTPUT);

  // Configura la interrupción externa en el pin del pulsador
  attachInterrupt(digitalPinToInterrupt(PIN_PULSADOR), activarPasoPeatón, RISING);
}
```

- `setup()` realiza la configuración inicial del programa. Inicializa la comunicación serial y configura los pines de los LEDs como salidas. Luego, configura una interrupción externa en el pin del pulsador que se activa en el flanco de subida (cuando el pulsador se presiona).

```cpp
void loop() {
  int fotoReceptor = analogRead(PIN_LDR);
  Serial.println(fotoReceptor);

  if (!secuenciaPeatón) {
    // Secuencia de noche
    if (fotoReceptor > LIMITE_CAMBIO) {
      // ... (secuencia de noche)
    } 
    // Secuencia de día
    else {
      // ... (secuencia de día)
    }
  } 
  else { // Secuencia del peatón
    // ... (secuencia del peatón)
  }
  
  delay(100); // Delay por estabilidad
}
```

- `loop()` es el bucle principal del programa. En función de si se está ejecutando la secuencia normal o la secuencia del peatón, se controla el estado de los LEDs de acuerdo con la luz ambiental y las acciones del pulsador.

## Uso del Código

Este código permite controlar un semáforo con interrupciones externas y adaptar la secuencia del semáforo en función de la luz ambiente. Además, incluye una secuencia especial para el paso del peatón activada por el pulsador. Puedes utilizar este código como punto de partida para proyectos que requieran semáforos inteligentes o sistemas de control de tráfico. Asegúrate de conectar los LEDs, el sensor de luz y el pulsador de acuerdo con las especificaciones de tu proyecto.