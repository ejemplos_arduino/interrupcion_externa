/**
 * Este código controla los leds de un semáforo
 * haciendo uso de las interrupciones externas.
 * 
 * Para ello se usa un pulsador que producirá la
 * interrupción para poner la luz verde durante un
 * tiempo y pase el peatón. Luego volverá a ejecutar
 * la secuencia normal del semáforo, ya sea de día o
 * de noche.
 */

const int LED_ROJO = 6; // Pin del led rojo
const int LED_AMARILLO = 5; // Pin del led amarillo
const int LED_VERDE = 4; // Pin del led verde

const int PIN_LDR = A0; // Pin lectura del LDR
const int PIN_PULSADOR = 2; // Pin lectura del pulsador
const int VELOCIDAD_CAMBIO = 500; // Tiempo de espera al cambiar led
const int LIMITE_CAMBIO = 900; // Límite para distinguir entre día y noche

bool secuenciaPeaton = false; // Indica si está activa la secuencia del peatón

// Función para cambiar el estado de los LEDs de manera más eficiente
void cambiarEstadoLEDs(bool rojo, bool amarillo, bool verde) {
  digitalWrite(LED_ROJO, rojo);
  digitalWrite(LED_AMARILLO, amarillo);
  digitalWrite(LED_VERDE, verde);
}

// Función de interrupción
void activarPasoPeaton() {
  Serial.println("INTERRUPCION!!");
  secuenciaPeaton = true;
}

void setup() {
  Serial.begin(9600); // Inicializa la consola
  
  pinMode(LED_ROJO, OUTPUT);
  pinMode(LED_AMARILLO, OUTPUT);
  pinMode(LED_VERDE, OUTPUT);

  // Configura la interrupción externa en el pin del pulsador
  attachInterrupt(digitalPinToInterrupt(PIN_PULSADOR), activarPasoPeaton, RISING);
}

void loop() {

  int fotoReceptor = analogRead(PIN_LDR);
  Serial.println(fotoReceptor);

  if (!secuenciaPeaton) {
    // Secuencia de noche
    if (fotoReceptor > LIMITE_CAMBIO){
      delay(1000);
      cambiarEstadoLEDs(LOW, HIGH, LOW);
      delay(VELOCIDAD_CAMBIO);
      cambiarEstadoLEDs(LOW, LOW, LOW);
      delay(VELOCIDAD_CAMBIO);
    } 
    // Secuencia de día
    else {
      cambiarEstadoLEDs(HIGH, LOW, LOW);
    }
  } 
  else { // Secuencia del peatón
    cambiarEstadoLEDs(LOW, LOW, HIGH);
    delay(3000);
    // Parpadeo antes de volver a la secuencia normal
    int espera = 300;
    for (int i=0; i<5; i++) {
      cambiarEstadoLEDs(LOW, LOW, LOW);
      delay(espera);
      cambiarEstadoLEDs(LOW, LOW, HIGH);
      delay(espera);
      espera -= 30; // Acelera el parpadeo del led
    }
    // Activa led amarillo de precaución
    cambiarEstadoLEDs(LOW, HIGH, LOW);
    delay(1000);
    secuenciaPeaton = false;
  }
  
  delay(100); // delay por estabilidad
}
